﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour
{
    public Material thisMaterial;
    public GameObject infoRects;
    public GameObject ObjectToShow;

    public Vector4 intiVector;
    private Vector4 planePos;

    public float valueSum, valueSub;
    public float add, sub;

    public bool startSum;

    void Start()
    {
        planePos = intiVector;
    }

    public void TurnOnObjects()
    {
        StopAllCoroutines();
        if (startSum) StartCoroutine(SumValueX(valueSum)); else StartCoroutine(SubstractValueX(valueSum));
    }

    public void TurnOffObjects()
    {
        StopAllCoroutines();
        if (startSum) StartCoroutine(SubstractValueX(valueSub)); else StartCoroutine(SumValueX(valueSub));  
    }


    IEnumerator SumValueX(float newValue)
    {
        //Debug.Log("SUMANDO !! A  : " + newValue + "   BY  : " + add + "   DESDE : " + planePos.x);
        float valueChange = add;
        
        while (planePos.x < newValue)
        {
            yield return null;
            planePos.x += valueChange;
            thisMaterial.SetVector("_Plane", planePos);
        }
        if (startSum) TurnOtherObjects(true); else TurnOtherObjects(false);
    }

    IEnumerator SubstractValueX(float newValue) 
    {
        //Debug.Log("RESTANDOOO !! A  : " + newValue + "   BY  : " + sub + "   DESDE : " + planePos.x);
        float valueChange = sub; 

        while (planePos.x > newValue)
        {      
            yield return null;
            planePos.x -= valueChange;
            thisMaterial.SetVector("_Plane", planePos);
        }
        if (startSum) TurnOtherObjects(false); else TurnOtherObjects(true);
    }

    void TurnOtherObjects(bool onOff)
    {
        infoRects.SetActive(onOff);
        ObjectToShow.SetActive(onOff);
    }

}
