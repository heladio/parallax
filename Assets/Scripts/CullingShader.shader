﻿
Shader "Custom/CullingShader"
{
	Properties{
		   _Color("Color", Color) = (1,1,1,1)
		   _MainTex("Albedo (RGB)", 2D) = "white" {}
		   _Glossiness("Smoothness", Range(0,1)) = 0.5
		   _Metallic("Metallic", Range(0,1)) = 0.0

		   _ClippingCentre("Clipping Centre", Vector) = (0,0,0,0)
		   _Plane("Plane", Vector) = (1,0.7,0.7,0)
			   //_offset ("offset",  Range(-1.5,1.5)) = 1
			   [Toggle] _invert("invert", Float) = 0
	}
		SubShader{
			Tags { "RenderType" = "Opaque" "Queue" = "Transparent"}
			LOD 200
			Cull off
			CGPROGRAM
			   #pragma surface surf Standard fullforwardshadows

			   #pragma target 3.0

			   sampler2D _MainTex;

			   struct Input {
				   float2 uv_MainTex;
				   float3 worldPos;
			   };

			   half _Glossiness;
			   half _Metallic;
			   fixed4 _Color;
			   float _invert;
			   uniform float _offset;

			   uniform float3 _ClippingCentre;
			   uniform float3 _Plane;

			  
			   UNITY_INSTANCING_BUFFER_START(Props)
			   UNITY_INSTANCING_BUFFER_END(Props)

			   void surf(Input IN, inout SurfaceOutputStandard o) {

				   if ((_offset - dot((IN.worldPos - _ClippingCentre),_Plane))*(1 - 2 * _invert) < 0) discard;

				   fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				   o.Albedo = c.rgb;
				   // Metallic and smoothness come from slider variables
				   o.Metallic = _Metallic;
				   o.Smoothness = _Glossiness;
				   o.Alpha = c.a;
			   }
			   ENDCG
		   }
			   FallBack "Diffuse"
}
