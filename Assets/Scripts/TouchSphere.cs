﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchSphere : MonoBehaviour
{
    public GameObject[] TurnOffObjs;
    public GameObject Lines;

    private bool waitForAnim;

    void OnMouseDown()
    {
        if (Lines == null || waitForAnim) return;

        if (this.gameObject.name == "Sphere")
        {
            for (int i = 0; i < TurnOffObjs.Length; i++)
            {
                TurnOffObjs[i].SetActive(!TurnOffObjs[i].activeSelf);
            }

            if (Lines.activeSelf == false) { Lines.SetActive(true); StartCoroutine("TurnOnRutine"); }
            else { StartCoroutine("TurnOffRutine"); }
        }

    }

    IEnumerator TurnOnRutine()
    {
        waitForAnim = true;
        yield return null;
        Lines.GetComponent<ChangeMaterial>().TurnOnObjects();
        yield return new WaitForSeconds(1);
        waitForAnim = false;
    }

    IEnumerator TurnOffRutine()
    {
        waitForAnim = true;
        Lines.GetComponent<ChangeMaterial>().TurnOffObjects();
        yield return new WaitForSeconds(1.2f);
        Lines.SetActive(false);
        waitForAnim = false;
    }
}
