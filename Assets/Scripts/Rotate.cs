﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{

    public bool turnX;
    public bool turnY;
    public bool turnZ;

    private Quaternion posIncial;

    private bool notStart = false;

    void Start()
    {
        notStart = true;
        posIncial = transform.rotation;
    }

    void OnEnable()
    {
        if (notStart)
        {
            transform.rotation = posIncial;
        }
    }


    void OnMouseDrag()
    {
        if (turnX)
        {
            float rotX = Input.GetAxis("Mouse X") * 80 * Mathf.Deg2Rad;
            transform.Rotate(Vector3.up, -rotX);
        }

        if (turnY)
        {
            float rotY = Input.GetAxis("Mouse Y") * 80 * Mathf.Deg2Rad;
            transform.Rotate(Vector3.right, rotY);
        }

        if (turnZ)
        {
            float rotZ = Input.GetAxis("Mouse X") * 80 * Mathf.Deg2Rad;
            transform.Rotate(Vector3.back, rotZ);
        }
    }
}
